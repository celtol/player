//wywołanie tunelu pomiędzy stronami
app = require("electron").remote.app;
app.Event.on("controller", v => eval(v));
//tablice dotyczące mediów
var interception;
var filepath;
var medialist = [];
var order = []
var current;
var count = 0;
var controlcount;
var reapeatstate = 0;
var currentstamp = 0;

$(function() {
		$('#list').selectable({
				cancel: '.handle',
		}).sortable({
				items: "> li",
				handle: '.handle',
				helper: function(event, item) {
						if (!item.hasClass('ui-selected')) {
								item.parent().children('.ui-selected').removeClass('ui-selected');
								item.addClass('ui-selected');
						}

						var selected = item.parent().children('.ui-selected').clone();
						item.data('multidrag', selected).siblings('.ui-selected').remove();
						return $('<li/>').append(selected);
				},
				change: function(event, ui) {
						order = $("#list").sortable('toArray', { attribute: 'value' });
				},
				start: function(event, ui) {
						order = $("#list").sortable('toArray', { attribute: 'value' });
				},
				stop: function(event, ui) {
						var selected = ui.item.data('multidrag');
						ui.item.after(selected);
						ui.item.remove();
						order = $("#list").sortable('toArray', { attribute: 'value' });
				},
				update: function(event, ui) {
						order = $("#list").sortable('toArray', { attribute: 'value' });
				}
		});
});


$("#list").on('contextmenu', '.ui-selected', function(e) {
		e.preventDefault();
		$("#effectsmodal").modal('show')
		$('.modal-backdrop').removeClass("modal-backdrop");
		citem = parseInt(document.getElementsByClassName('ui-selected')[0].id)
		var geturl = medialist.find(i => i.id == citem)

		document.getElementById("videopreview").src = geturl.url.replace(/\\/g, "\\\\");
		document.getElementById("videopreview").play();

		itemBlur = geturl.effects.blur
		itemBrightness = geturl.effects.brightness
		itemContrast = geturl.effects.contrast
		itemGrayscale = geturl.effects.grayscale
		itemHuerotate = geturl.effects.huerotate
		itemInvert = geturl.effects.invert
		itemOpacity = geturl.effects.opacity
		itemSaturate = geturl.effects.saturate
		itemSepia = geturl.effects.sepia

		document.getElementById("blur").value = itemBlur
		document.getElementById("brightness").value = itemBrightness
		document.getElementById("contrast").value = itemContrast
		document.getElementById("grayscale").value = itemGrayscale
		document.getElementById("huerotate").value = itemHuerotate
		document.getElementById("invert").value = itemInvert
		document.getElementById("opacity").value = itemOpacity
		document.getElementById("saturate").value = itemSaturate
		document.getElementById("sepia").value = itemSepia
})





function addresscatch(event) {
		//przypisanie elementu listy do zmiennej
		list = document.getElementById("list")
		//sprawdzenie ilości wybranych plików
		interception = event.target.files.length;
		for (j = 0; j < interception; j++) {
				//pobranie ścieżki do plików
				filepath = event.target.files[j].path;
				extension = event.target.files[j].path.split(".").pop()
				if (extension == "jpg" || extension == "jpeg" || extension == "png") {
						//otwórz modal wybrania czasu
						console.log(filepath)
						$("#imagetimermodal").modal('show')
				} else if (extension == "mp4" || extension == "ogg" || extension == "ogg") {
						medialist.push({
								id: count,
								url: filepath,
								extension: extension,
								effects: {
										blur: 0,
										brightness: 100,
										contrast: 100,
										grayscale: 0,
										huerotate: 0,
										invert: 0,
										opacity: 100,
										saturate: 100,
										sepia: 0
								}
						})
						//dodawanie elementów do listy mediów
						//tworzenie elementu listy z wartością j
						li = document.createElement("li")
						li.value = count;
						li.setAttribute("id", "" + count + "")
						count++
						li.setAttribute("ondblclick", "selectandplay(value)")
						//li.setAttribute("class", "item ui-selectable");
						//tworzenie napisu z nazwą pliku
						span = document.createElement("span")
						span.setAttribute("class", "handle hidden sort-handle")
						t = document.createTextNode(filepath.replace(/^.*[\\\/]/, ''));
						t2 = document.createTextNode(filepath.replace(/^.*[\\\/]/, ''));
						//przypisanie elementów dzieci do rodziców	
						span.appendChild(t)
						li.appendChild(span)
						li.appendChild(t2)
						list.appendChild(li)
				}
				//wysłanie ścieżki do pliku do tablicy mediów

		}
		order = $("#list").sortable('toArray', { attribute: 'value' });

}
//funkcja wykonywana po podwójnym kliknięciu elementu listy
function selectandplay(value) {
		//pobranie wartości wybranego elementu
		selected = document.getElementById(value).value;
		var geturl = medialist.find(i => i.id == selected)

		itemBlur = geturl.effects.blur
		itemBrightness = geturl.effects.brightness
		itemContrast = geturl.effects.contrast
		itemGrayscale = geturl.effects.grayscale
		itemHuerotate = geturl.effects.huerotate
		itemInvert = geturl.effects.invert
		itemOpacity = geturl.effects.opacity
		itemSaturate = geturl.effects.saturate
		itemSepia = geturl.effects.sepia


		//przypisanie wartości wybranej do aktualnego wybranego medium
		current = selected;
		//aktualizacja listy odtwarzania
		order = $("#list").sortable('toArray', { attribute: 'value' });
		//wysłanie ścieżki do pliku oraz jego odtworzenie do podglądu i ekranu głównego
		document.getElementById("videopreview").src = geturl.url.replace(/\\/g, "\\\\");
		document.getElementById("videopreview").style.filter = "blur(" + itemBlur + "px) brightness(" + itemBrightness + "%) contrast(" + itemContrast + "%) grayscale(" + itemGrayscale + "%) hue-rotate(" + itemHuerotate + "deg) invert(" + itemInvert + "%) opacity(" + itemOpacity + "%) saturate(" + itemSaturate + "%) sepia(" + itemSepia + "%)"
		document.getElementById("videopreview").play();
		app.Event.emit("monitor", 'document.getElementById("video").src = "' + geturl.url.replace(/\\/g, "\\\\") + '"');

		app.Event.emit("monitor", 'document.getElementById("video").style.filter = "blur(' + itemBlur + 'px) brightness(' + itemBrightness + '%) contrast(' + itemContrast + '%) grayscale(' + itemGrayscale + '%) hue-rotate(' + itemHuerotate + 'deg) invert(' + itemInvert + '%) opacity(' + itemOpacity + '%) saturate(' + itemSaturate + '%) sepia(' + itemSepia + '%)"');

		app.Event.emit("monitor", 'document.getElementById("video").play()');
}
//funkcja odtwarzania załadowanego medium
function play() {
		//odtworzenie załadowanego medium
		app.Event.emit("monitor", 'document.getElementById("video").play()');
		document.getElementById("videopreview").play();
		//aktualizacja listy odtwarzania
		order = $("#list").sortable('toArray', { attribute: 'value' });
}
//funkcja pauzy medium
function pause() {
		app.Event.emit("monitor", 'document.getElementById("video").pause()');
		document.getElementById("videopreview").pause();
}
//funkcja zatrzymanania i załadowania od nowa medium
function stop() {
		app.Event.emit("monitor", 'document.getElementById("video").load()');
		document.getElementById("videopreview").load();
}
//funkcja przejścia do kolejnego medium w liście
function next() {
		//zmiana wartości elementu aktualnie odtwarzanego medium
		current = current + 1;
		//pobranie ilości elemntów listy mediów
		playlistlenght = order.length
		//sprawdzenie czy zwiększenie wybranego mediów nie przekracza ilości mediów w liście
		if (current >= playlistlenght) {
				current = 0;
		}
		//zmiana wartości wybranej
		selected = order[current]
		var geturl = medialist.find(i => i.id == selected)
		//załadowanie i odtworzenie mediów
		document.getElementById("videopreview").src = medialist[selected].url.replace(/\\/g, "\\\\");
		document.getElementById("videopreview").play();
		app.Event.emit("monitor", 'document.getElementById("video").src = "' + geturl.url.replace(/\\/g, "\\\\") + '"');
		app.Event.emit("monitor", 'document.getElementById("video").play()');
}
//funkcja przejścia do poprzedniego medium w liście
function previuos() {
		current = current - 1;
		playlistlenght = order.length
		if (current < 0) {
				current = order.length - 1;
		}
		selected = order[current]
		var geturl = medialist.find(i => i.id == selected)
		document.getElementById("videopreview").src = geturl.url.replace(/\\/g, "\\\\");
		document.getElementById("videopreview").play();
		app.Event.emit("monitor", 'document.getElementById("video").src = "' + geturl.url.replace(/\\/g, "\\\\") + '"');
		app.Event.emit("monitor", 'document.getElementById("video").play()');
}
//funkcja ustawiająca przycisk powtarzania
function repeat() {
		changeclicked = document.getElementById("repeat")
		if (reapeatstate == 0) {
				changeclicked.style.borderStyle = 'outset'
				changeclicked.style.borderColor = 'blue'
				reapeatstate = 1;
		} else if (reapeatstate == 1) {
				changeclicked.innerHTML = '<i class="fas fa-repeat-1"></i>'
				reapeatstate = 2;
		} else if (reapeatstate == 2) {
				changeclicked.innerHTML = '<i class="fas fa-repeat"></i>'
				changeclicked.style.borderStyle = 'outset'
				changeclicked.style.borderColor = 'buttonface'
				reapeatstate = 0;
		} else {
				reapeatstate = 0;
		}
}


//funkcja usuwająca element z list
function removeitem() {
		list = document.getElementById('list');
		delitemid = document.getElementsByClassName('ui-selected')[0].id;
		delete medialist[delitemid]
		medialist.splice(delitemid, 1)
		order.splice(delitemid, 1)
		deleteit = document.getElementsByClassName('ui-selected')[0]
		list.removeChild(deleteit)

}
//funkcja przewijająca do kolejnego elementu z listy gdy poprzedni się zakończy
function itemend() {
		if (reapeatstate == 0) {
				current = current + 1;
				//sprawdzenie czy istnieje kolejny element oraz zatrzymanie gdy kolejny nie istnieje i przejście do pierwszego elemntu listy
				playlistlenght = order.length
				if (current >= playlistlenght) {
						current = 0
						selected = order[current]
						var geturl = medialist.find(i => i.id == selected)
						app.Event.emit("monitor", 'document.getElementById("video").src = "' + geturl.url.replace(/\\/g, "\\\\") + '"');
						app.Event.emit("monitor", 'document.getElementById("video").load()');
						document.getElementById("videopreview").src = geturl.url.replace(/\\/g, "\\\\");
						document.getElementById("videopreview").load();
				} else {
						//przejście do kolejnego elementu listy mediów gdy istnieje
						selected = order[current]
						var geturl = medialist.find(i => i.id == selected)
						document.getElementById("videopreview").src = geturl.url.replace(/\\/g, "\\\\");
						document.getElementById("videopreview").play();
						app.Event.emit("monitor", 'document.getElementById("video").src = "' + geturl.url.replace(/\\/g, "\\\\") + '"');
						app.Event.emit("monitor", 'document.getElementById("video").play()');
				}
		} else if (reapeatstate == 1) {
				current = current + 1;
				//sprawdzenie czy istnieje kolejny element oraz zatrzymanie gdy kolejny nie istnieje i przejście do pierwszego elemntu listy
				playlistlenght = order.length
				if (current >= playlistlenght) {
						current = 0
						selected = order[current]
						var geturl = medialist.find(i => i.id == selected)
						app.Event.emit("monitor", 'document.getElementById("video").src = "' + geturl.url.replace(/\\/g, "\\\\") + '"');
						app.Event.emit("monitor", 'document.getElementById("video").play()');
						document.getElementById("videopreview").src = geturl.url.replace(/\\/g, "\\\\");
						document.getElementById("videopreview").play();
				} else {
						//przejście do kolejnego elementu listy mediów gdy istnieje
						selected = order[current]
						var geturl = medialist.find(i => i.id == selected)
						document.getElementById("videopreview").src = geturl.url.replace(/\\/g, "\\\\");
						document.getElementById("videopreview").play();
						app.Event.emit("monitor", 'document.getElementById("video").src = "' + geturl.url.replace(/\\/g, "\\\\") + '"');
						app.Event.emit("monitor", 'document.getElementById("video").play()');
				}
		} else if (reapeatstate == 2) {
				selected = order[current]
				var geturl = medialist.find(i => i.id == selected)
				document.getElementById("videopreview").src = geturl.url.replace(/\\/g, "\\\\");
				document.getElementById("videopreview").play();
				app.Event.emit("monitor", 'document.getElementById("video").src = "' + geturl.url.replace(/\\/g, "\\\\") + '"');
				app.Event.emit("monitor", 'document.getElementById("video").play()');
		}

}

function savefilters() {
		citem = parseInt(document.getElementsByClassName('ui-selected')[0].id)
		var geturl = medialist.find(i => i.id == citem)

		geturl.effects.blur = parseInt(document.getElementById("blur").value)
		geturl.effects.brightness = parseInt(document.getElementById("brightness").value)
		geturl.effects.contrast = parseInt(document.getElementById("contrast").value)
		geturl.effects.grayscale = parseInt(document.getElementById("grayscale").value)
		geturl.effects.huerotate = parseInt(document.getElementById("huerotate").value)
		geturl.effects.invert = parseInt(document.getElementById("invert").value)
		geturl.effects.opacity = parseInt(document.getElementById("opacity").value)
		geturl.effects.saturate = parseInt(document.getElementById("saturate").value)
		geturl.effects.sepia = parseInt(document.getElementById("sepia").value)

		closeandforgetefeects();
}

function closeandforgetefeects() {
		if (current === undefined) {
				document.getElementById("videopreview").src = ""
		} else {
				app.Event.emit("monitor", "getcurrenttime()")
				setTimeout(function() {
						var geturl = medialist.find(i => i.id == current)
						document.getElementById("videopreview").src = geturl.url.replace(/\\/g, "\\\\");
						document.getElementById("videopreview").play();
						document.getElementById("videopreview").currentTime = currentstamp
				}, 1);
		}


		$("#effectsmodal").modal('hide')
}

function imagesavetimer() {
		timer = document.getElementById("timer").value
		medialist.push({
				id: count,
				url: filepath,
				extension: extension,
				time: timer,
				effects: {
						blur: 0,
						brightness: 100,
						contrast: 100,
						grayscale: 0,
						huerotate: 0,
						invert: 0,
						opacity: 100,
						saturate: 100,
						sepia: 0
				}
		})
		//dodawanie elementów do listy mediów
		//tworzenie elementu listy z wartością j
		li = document.createElement("li")
		li.value = count;
		li.setAttribute("id", "" + count + "")
		count++
		li.setAttribute("ondblclick", "selectandplay(value)")
		//li.setAttribute("class", "item ui-selectable");
		//tworzenie napisu z nazwą pliku
		span = document.createElement("span")
		span.setAttribute("class", "handle hidden sort-handle")
		t = document.createTextNode(filepath.replace(/^.*[\\\/]/, ''));
		t2 = document.createTextNode(filepath.replace(/^.*[\\\/]/, ''));
		//przypisanie elementów dzieci do rodziców	
		span.appendChild(t)
		li.appendChild(span)
		li.appendChild(t2)
		list.appendChild(li)
		$("#imagetimermodal").modal('hide')
}
