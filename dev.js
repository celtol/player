'use strict';
const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
var EventEmitter = require("events").EventEmitter; //Inicjalizacja modułu wydarzeń
// Adds debug features like hotkeys for triggering dev tools and reload
require('electron-debug')();
app.commandLine.appendSwitch('autoplay-policy', 'no-user-gesture-required');
// Prevent window being garbage collected
let controlWindow;
let secondWindow;
app.Event = new EventEmitter(); 


function onClosed() {
	// Dereference the window
	// For multiple windows store them in an array
	controlWindow = null;
	app.quit();
}

function createControlWindow() {
	const win = new electron.BrowserWindow({
		width: 800,
		height: 600
	});

	win.loadURL(`file://${__dirname}/src/main.html`);
	win.on('closed', onClosed);

	return win;
}

function createTestDisplayWindow() { //funkcja drugiego okna w celu testów
	const win = new electron.BrowserWindow({
		width: 800,
		height: 600
	});

	win.loadURL(`file://${__dirname}/src/display.html`);
	win.on('closed', onClosed);

	return win;
}


app.on('window-all-closed', () => {
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

app.on('activate', () => {
	if (!controlWindow) {
		controlWindow = createControlWindow();
	}
});

app.on('ready', () => {
var electronScreen = electron.screen;
  /*var displays = electronScreen.getAllDisplays();
  var externalDisplay = null;
  console.log(displays);
  for (var i in displays) {
    if (displays[i].bounds.x != 0 || displays[i].bounds.y != 0) {
      externalDisplay = displays[i];
      break;
    }
  }

  if (externalDisplay) {
    secondWindow = new electron.BrowserWindow({
		frame: false,
		x: externalDisplay.bounds.x + 50,
      	y: externalDisplay.bounds.y + 50,
      	fullscreen: true,
	});

	secondWindow.loadURL(`file://${__dirname}/src/display.html`);
	secondWindow.on('closed', onClosed);
	
  }*/
  createControlWindow = createControlWindow(); //wywołanie głównego okna kontrolnego
  createTestDisplayWindow = createTestDisplayWindow(); //wywołanie okna do testów
});

