# Dual Screen Player 

> This is my application for displaying a video on second screen when maintaining on first controls


## Usage

### Install
First you need to copy the repo

```
git clone https://gitlab.com/celtol/player.git
```
Next go to the app directory
```
$ cd player
```
Next initzialize the project dependencies
```
$ yarn
```

### Run

Start live project
```
$ yarn run live
```

Start debug project (live project with ability to launch console)
```
$ yarn run debug
```

Start dev project
```
$ yarn run dev
```